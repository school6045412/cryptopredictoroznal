import pymongo
import json
from datetime import datetime, date, timedelta, timezone
import time
import numpy as np
import pandas as pd
import dateutil.parser
import re
import nltk
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk import ngrams
import math
import sys
from sklearn.feature_extraction.text import TfidfVectorizer, TfidfTransformer
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from wordcloud import WordCloud, STOPWORDS

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["twitter"]
mycol = mydb["python_test"]
price_col = mydb["bitcoin_price_hourly"]
wordnet_lemmatizer = WordNetLemmatizer()
total_vector = []
sentiment_list = []
time_list = []
total_sentiment = []


def compute_tf(doc):
    tf_dict = {}
    for word in doc:
        if word in tf_dict:
            tf_dict[word] += 1
        else:
            tf_dict[word] = 1
    # Computes tf for each word
    for word in tf_dict:
        tf_dict[word] = tf_dict[word] / len(doc)
    return tf_dict


def default(o):
    if isinstance(o, (date, datetime)):
        return o.isoformat()


def compute_idf(doc_num, count_dict):
    idf_dict = {}
    index = 0
    for word in count_dict:
        idf_dict[word] = math.log(doc_num / count_dict[word])

        index = index + 1
        sys.stdout.flush()
        sys.stdout.write("\rIDF`s computed: " + "{:.2%}".format((index / len(count_dict))))

    sys.stdout.flush()
    sys.stdout.write("\rIDF`s computed: " + "{:.2%}\n".format((index / len(count_dict))))
    return idf_dict


def compute_count_dict(tf_dict):
    count_dict = {}
    # Run through each review's tf dictionary and increment countDict's (word, doc) pair
    print("Computing word occurances in all tweets")
    index = 0
    for tf in tf_dict:
        for word in tf:
            if word in count_dict:
                count_dict[word] += 1
            else:
                count_dict[word] = 1

        index = index + 1
        sys.stdout.flush()
        sys.stdout.write("\rTF`s in TF dictionary processed: " + "{:.2%}".format((index / len(tf_dict))))

    sys.stdout.flush()
    sys.stdout.write("\rTF`s in TF dictionary processed: " + "{:.2%}\n".format((index / len(tf_dict))))
    return count_dict


def compute_total_vector(tfs, idfs):
    vector = {}
    for word in tfs:
        vector[word] = tfs[word] * idfs[word]
    return vector


def join_tweets(tweets):
    for tweet in tweets:
        doc_joined = " ".join(tweet)
    return doc_joined


def clean_tweets(tweets):
    processed = ""
    for tweet in tweets["text"]:
        try:
            proc_tweet = " ".join([word for word in tweet.split()
                                          if 'http' not in word and '@' not in word and '<' not in word and 'pic.twitter.com' not in word])
            proc_tweet = re.sub('[-:!@#$^()".;,?&=]', ' ', proc_tweet)
            proc_tweet = re.sub('  ', ' ', proc_tweet)
            proc_tweet = proc_tweet.lower()
            processed = processed + " " + proc_tweet
        except StopIteration:
            print("Error occurred while processing tweets!")
    return processed


def process_tweets(tweets):
    processed = []
    for tweet in tweets["text"]:
        try:
            proc_tweet = " ".join([word for word in tweet.split()
                                          if 'http' not in word and '@' not in word and '<' not in word and 'pic.twitter.com' not in word])
            proc_tweet = re.sub('[-:!@#$^()".;,?&=]', ' ', proc_tweet)
            proc_tweet = re.sub('  ', ' ', proc_tweet)
            proc_tweet = proc_tweet.lower()
            processed.extend(proc_tweet.split())
        except StopIteration:
            print("Error occurred while processing tweets!")
    return processed


def process_single_tweet(tweet):
    try:
        proc_tweet = " ".join([word for word in tweet.split()
                               if
                               'http' not in word and '@' not in word and '#' not in word and '<' not in word and 'pic.twitter.com' not in word])
        proc_tweet = re.sub('[-:!@#^()".;,?&=]', ' ', proc_tweet)
        #proc_tweet = re.sub('  ', ' ', proc_tweet)
        #proc_tweet = proc_tweet.lower()
    except StopIteration:
        print("Error occurred while processing tweets!")
    return proc_tweet

#db.getCollection('python_test').aggregate(
#[
#    {
#        $group: {
#            _id: {$convert: {input: {$divide: ["$date", 3600]}, to: "int"}},
#            text: {$push: "$text"}
#    }
#],  {allowDiskUse: true})

#db.getCollection('python_test').aggregate(
#[
#    {
#        $group: {
#            _id: {$subtract: ["$date", { $convert: {input: {$mod: ["$date", 3600]}, to: "int"}}]},
#            text: {$push: "$text"}
#    }
#],  {allowDiskUse: true})


def load_tweets():
    pipeline = [{"$group": {"_id": {"$subtract": ["$date", { "$convert": {"input": {"$mod": ["$date", 3600]}, "to": "int"}}]}, "text": {"$push": "$text"}}}, {"$limit": 10}]
    doc_list = []

    try:
        print("Fetching tweets...")
        my_doc = list(mycol.aggregate(pipeline, allowDiskUse=True))

        sid_obj = SentimentIntensityAnalyzer()
        for row in my_doc:
            prices = list(price_col.find({"time": {"$gt": row["_id"]}}).sort("time", 1).limit(48))
            print(prices)

            count = 0
            hour_sentiment = {"time": row["_id"], "count": 0}

            #time_lema = {"time": row["_id"], "words": []}
            for tweet in row["text"]:
                processed = process_single_tweet(tweet)
                sentiment_dict = sid_obj.polarity_scores(processed)

                # print(tweet)
                # print("sentence was rated as ", sentiment_dict['neg'] * 100, "% Negative")
                # print("sentence was rated as ", sentiment_dict['neu'] * 100, "% Neutral")
                # print("sentence was rated as ", sentiment_dict['pos'] * 100, "% Positive")

                if 0.5 < sentiment_dict['compound'] >= 0.75:
                    count = count + sentiment_dict['compound']
                    for sentiment in sentiment_list:
                        if sentiment["sentiment"] == "buy":
                            sentiment["tweets"].append({"tweet": tweet, "score": sentiment_dict["compound"], "time": row["_id"]})

                if sentiment_dict['compound'] > 0.75:
                    count = count + sentiment_dict['compound']
                    for sentiment in sentiment_list:
                        if sentiment["sentiment"] == "strong_buy":
                            sentiment["tweets"].append({"tweet": tweet, "score": sentiment_dict["compound"], "time": row["_id"]})

                if -0.5 > sentiment_dict['compound'] >= -0.75:
                    count = count + sentiment_dict['compound']
                    for sentiment in sentiment_list:
                        if sentiment["sentiment"] == "sell":
                            sentiment["tweets"].append({"tweet": tweet, "score": sentiment_dict["compound"], "time": row["_id"]})

                if sentiment_dict['compound'] < -0.75:
                    count = count + sentiment_dict['compound']
                    for sentiment in sentiment_list:
                        if sentiment["sentiment"] == "strong_sell":
                            sentiment["tweets"].append({"tweet": tweet, "score": sentiment_dict["compound"], "time": row["_id"]})

            hour_sentiment["count"] = count
            total_sentiment.append(hour_sentiment)
                #tokens = nltk.word_tokenize(processed)
                #lower_case = [l.lower() for l in tokens]
                #filtered_result = list(filter(lambda l: l not in STOPWORDS, lower_case))
                #emmas = [wordnet_lemmatizer.lemmatize(t) for t in filtered_result]
                #time_lema["words"].append(lemmas)

            #time_list.append(time_lema)

        #vectorizer = TfidfTransformer(smooth_idf=False, sublinear_tf=False, use_idf=True)
        #computed_vector = vectorizer.fit_transform(doc_list)
        #print(computed_vector.todense().tolist())
        #print(vectorizer.get_feature_names())

        #for row in my_doc:
        #    doc_list.append(process_tweets(row))
        #    count = count + 1
        #    if count % 50 == 0:
        #        sys.stdout.flush()
        #        sys.stdout.write("\rDocuments processed: " + "{:.2%}".format(count / len(my_doc)) + " " + str(count) + " of " + str(len(my_doc)))
    except StopIteration:
        print("Error loading tweets from MongoDB!")
    #sys.stdout.flush()
    #sys.stdout.write("\rDocuments processed: " + "{:.2%}\n".format(count / len(my_doc)))
    return doc_list


if __name__ == "__main__":
    sentiment_list.append({"sentiment": "buy", "tweets": []})
    sentiment_list.append({"sentiment": "strong_buy", "tweets": []})
    sentiment_list.append({"sentiment": "sell", "tweets": []})
    sentiment_list.append({"sentiment": "strong_sell", "tweets": []})

    docs = load_tweets()

    for item in total_sentiment:
        print(item)



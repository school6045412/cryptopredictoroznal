import requests
import datetime
from datetime import timezone
import pymongo
import json


myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["twitter"]
mycol_daily = mydb["bitcoin_price_daily"]
mycol_hourly = mydb["bitcoin_price_hourly"]
mycol_4hourly = mydb["bitcoin_price_4hourly"]
mycol_half = mydb["bitcoin_price_12hourly"]
api_key = "90e8ffb6df4b06c4aef14197715c302f48e8e2bff06538620812ed0ef3a67c53"
target_date = datetime.datetime(2017, 1, 10)
target_ts = round(target_date.replace(tzinfo=timezone.utc).timestamp())

def load_price_daily():
    mydoc = list(mycol_daily.find({}).sort("time", 1).limit(1))
    latest_ts = round(datetime.datetime.now().replace(tzinfo=timezone.utc).timestamp())

    if mydoc:
        latest_ts = mydoc[0]["time"]

    while(not mydoc or latest_ts > target_ts):
        url = "https://min-api.cryptocompare.com/data/histoday?fsym=BTC&tsym=USD&toTs=" + str(latest_ts) + "&limit=2000&api_key=" + api_key
        daily_prices = requests.get(url)
        data = json.loads(daily_prices.text)
        mycol_daily.insert_many(data["Data"])

        mydoc = list(mycol_daily.find({}).sort("time", 1).limit(1))
        latest_ts = mydoc[0]["time"]


def load_price_hourly(aggregation, my_col):
    mydoc = list(my_col.find({}).sort("time", 1).limit(1))
    latest_ts = round(datetime.datetime.now().replace(tzinfo=timezone.utc).timestamp())

    if mydoc:
        latest_ts = mydoc[0]["time"]

    while(not mydoc or latest_ts > target_ts):
        url = "https://min-api.cryptocompare.com/data/histohour?fsym=BTC&tsym=USD&toTs=" + str(latest_ts) + "&limit=2000&aggregate=" + str(aggregation) + "&api_key=" + api_key
        daily_prices = requests.get(url)
        data = json.loads(daily_prices.text)
        my_col.insert_many(data["Data"])

        mydoc = list(my_col.find({}).sort("time", 1).limit(1))
        latest_ts = mydoc[0]["time"]


if __name__ == "__main__":
    # load_price_daily()
    # load_price_hourly(1, mycol_hourly)
    # load_price_hourly(4, mycol_4hourly)
    load_price_hourly(12, mycol_half)

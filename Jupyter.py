#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pymongo
import json
from datetime import datetime, date, timedelta, timezone
import time
import copy
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import dateutil.parser
import re
import nltk
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk import ngrams
import math
import sys
from sklearn.feature_extraction.text import TfidfVectorizer, TfidfTransformer
from sklearn.svm import SVC
from sklearn.metrics import classification_report, confusion_matrix
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from wordcloud import WordCloud, STOPWORDS

# In[2]:


myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["twitter"]
mycol = mydb["python_test"]
my_testcol = mydb["python_learn"]
price_col = mydb["bitcoin_price_hourly"]
price_col_d = mydb["bitcoin_price_daily"]
price_col_hd = mydb["bitcoin_price_12hourly"]
wordnet_lemmatizer = WordNetLemmatizer()
total_vector = []
sentiment_list = []
time_list = []
total_sentiment = []
total_sentiment2 = []
verdicts = []

test_total_sentiment = []
test_total_sentiment2 = []
test_verdicts = []


# In[3]:


def compute_tf(doc):
    tf_dict = {}
    for word in doc:
        if word in tf_dict:
            tf_dict[word] += 1
        else:
            tf_dict[word] = 1
    # Computes tf for each word
    for word in tf_dict:
        tf_dict[word] = tf_dict[word] / len(doc)
    return tf_dict


def default(o):
    if isinstance(o, (date, datetime)):
        return o.isoformat()


def compute_idf(doc_num, count_dict):
    idf_dict = {}
    index = 0
    for word in count_dict:
        idf_dict[word] = math.log(doc_num / count_dict[word])

        index = index + 1
        sys.stdout.flush()
        sys.stdout.write("\rIDF`s computed: " + "{:.2%}".format((index / len(count_dict))))

    sys.stdout.flush()
    sys.stdout.write("\rIDF`s computed: " + "{:.2%}\n".format((index / len(count_dict))))
    return idf_dict


def compute_count_dict(tf_dict):
    count_dict = {}
    # Run through each review's tf dictionary and increment countDict's (word, doc) pair
    print("Computing word occurances in all tweets")
    index = 0
    for tf in tf_dict:
        for word in tf:
            if word in count_dict:
                count_dict[word] += 1
            else:
                count_dict[word] = 1

        index = index + 1
        sys.stdout.flush()
        sys.stdout.write("\rTF`s in TF dictionary processed: " + "{:.2%}".format((index / len(tf_dict))))

    sys.stdout.flush()
    sys.stdout.write("\rTF`s in TF dictionary processed: " + "{:.2%}\n".format((index / len(tf_dict))))
    return count_dict


def compute_total_vector(tfs, idfs):
    vector = {}
    for word in tfs:
        vector[word] = tfs[word] * idfs[word]
    return vector


# In[4]:


def join_tweets(tweets):
    for tweet in tweets:
        doc_joined = " ".join(tweet)
    return doc_joined


def clean_tweets(tweets):
    processed = ""
    for tweet in tweets["text"]:
        try:
            proc_tweet = " ".join([word for word in tweet.split()
                                   if
                                   'http' not in word and '@' not in word and '<' not in word and 'pic.twitter.com' not in word])
            proc_tweet = re.sub('[-:!@#$^()".;,?&=]', ' ', proc_tweet)
            proc_tweet = re.sub('  ', ' ', proc_tweet)
            proc_tweet = proc_tweet.lower()
            processed = processed + " " + proc_tweet
        except StopIteration:
            print("Error occurred while processing tweets!")
    return processed


def process_tweets(tweets):
    processed = []
    for tweet in tweets["text"]:
        try:
            proc_tweet = " ".join([word for word in tweet.split()
                                   if
                                   'http' not in word and '@' not in word and '<' not in word and 'pic.twitter.com' not in word])
            proc_tweet = re.sub('[-:!@#$^()".;,?&=]', ' ', proc_tweet)
            proc_tweet = re.sub('  ', ' ', proc_tweet)
            proc_tweet = proc_tweet.lower()
            processed.extend(proc_tweet.split())
        except StopIteration:
            print("Error occurred while processing tweets!")
    return processed


# In[5]:


def process_single_tweet(tweet):
    try:
        proc_tweet = " ".join([word for word in tweet.split()
                               if
                               'http' not in word and '@' not in word and '#' not in word and '<' not in word and 'pic.twitter.com' not in word])
        proc_tweet = re.sub('[-:!@#^()".;,?&=]', ' ', proc_tweet)
        # proc_tweet = re.sub('  ', ' ', proc_tweet)
        # proc_tweet = proc_tweet.lower()
    except StopIteration:
        print("Error occurred while processing tweets!")
    return proc_tweet


# In[6]:


def load_test_tweets():
    pipeline = [{"$group": {
        "_id": {"$subtract": ["$date", {"$convert": {"input": {"$mod": ["$date", 43200]}, "to": "int"}}]},
        "text": {"$push": "$text"}}}, {"$limit": 20}]
    doc_list = []

    try:
        print("Fetching tweets...")
        my_doc = list(my_testcol.aggregate(pipeline, allowDiskUse=True))

        sid_obj = SentimentIntensityAnalyzer()
        for row in my_doc:
            prices = iter(list(price_col_hd.find({"time": {"$gte": row["_id"]}}).sort("time", 1).limit(2)))
            cur_price = next(prices)
            price = next(prices)
            cnt = 0
            count = 0
            verdict = 1

            max = (cur_price["high"] / cur_price["open"]) - 1
            if abs((cur_price["low"] / cur_price["open"]) - 1) > max:
                max = (cur_price["low"] / cur_price["open"]) - 1

            if (price["high"] / price["open"]) - 1 >= abs((price["low"] / price["open"]) - 1):
                verdict = 1
            else:
                verdict = -1
            test_verdicts.append(verdict)

            for tweet in row["text"]:
                processed = process_single_tweet(tweet)
                sentiment_dict = sid_obj.polarity_scores(processed)

                if 0.5 < sentiment_dict['compound'] >= 0.75:
                    count = count + sentiment_dict['compound']
                    cnt = cnt + 1

                if sentiment_dict['compound'] > 0.75:
                    count = count + sentiment_dict['compound']
                    cnt = cnt + 1

                if -0.5 > sentiment_dict['compound'] >= -0.75:
                    count = count + sentiment_dict['compound']
                    cnt = cnt + 1

                if sentiment_dict['compound'] < -0.75:
                    count = count + sentiment_dict['compound']
                    cnt = cnt + 1

            #             hour_sentiment["count"] = (count * 100) / len(row["text"])
            test_total_sentiment.append([(count * 100) / len(row["text"]), max])

            #             hour_sentiment["count"] = ((count * 100) / cnt)
            test_total_sentiment2.append([((count * 100) / cnt), max])
            # tokens = nltk.word_tokenize(processed)
            # lower_case = [l.lower() for l in tokens]
            # filtered_result = list(filter(lambda l: l not in STOPWORDS, lower_case))
            # emmas = [wordnet_lemmatizer.lemmatize(t) for t in filtered_result]
            # time_lema["words"].append(lemmas)

            # time_list.append(time_lema)

    except StopIteration:
        print("Error loading tweets from MongoDB!")
    # sys.stdout.flush()
    # sys.stdout.write("\rDocuments processed: " + "{:.2%}\n".format(count / len(my_doc)))
    return 0


# In[7]:


def load_tweets(from_ts):
    pipeline = [{"$match": {"date": {"$gte": from_ts}}}, {
        "$group": {"_id": {"$subtract": ["$date", {"$convert": {"input": {"$mod": ["$date", 43200]}, "to": "int"}}]},
                   "text": {"$push": "$text"}}}, {"$limit": 60}]
    doc_list = []

    try:
        print("Fetching tweets...")
        my_doc = list(mycol.aggregate(pipeline, allowDiskUse=True))

        sid_obj = SentimentIntensityAnalyzer()
        for row in my_doc:
            prices = iter(list(price_col_hd.find({"time": {"$gte": row["_id"]}}).sort("time", 1).limit(2)))
            cur_price = next(prices)
            price = next(prices)
            # print(price)
            cnt = 0
            count = 0
            verdict = 1

            max = (cur_price["high"] / cur_price["open"]) - 1
            if abs((cur_price["low"] / cur_price["open"]) - 1) > max:
                max = (cur_price["low"] / cur_price["open"]) - 1

            if (price["high"] / price["open"]) - 1 >= abs((price["low"] / price["open"]) - 1):
                verdict = 1
            else:
                verdict = -1
            verdicts.append(verdict)
            #           hour_sentiment = {"time": row["_id"], "count": 0, "buy": (price["high"] / price["open"]) - 1, "sell": (price["open"] / price["low"]) - 1, "spread": max}
            #             hour_sentiment = {"count": 0, "spread": max}

            # time_lema = {"time": row["_id"], "words": []}
            for tweet in row["text"]:
                processed = process_single_tweet(tweet)
                sentiment_dict = sid_obj.polarity_scores(processed)

                if 0.5 < sentiment_dict['compound'] >= 0.75:
                    count = count + sentiment_dict['compound']
                    cnt = cnt + 1
                    for sentiment in sentiment_list:
                        if sentiment["sentiment"] == "buy":
                            sentiment["tweets"].append(
                                {"tweet": tweet, "score": sentiment_dict["compound"], "time": row["_id"]})

                if sentiment_dict['compound'] > 0.75:
                    count = count + sentiment_dict['compound']
                    cnt = cnt + 1
                    for sentiment in sentiment_list:
                        if sentiment["sentiment"] == "strong_buy":
                            sentiment["tweets"].append(
                                {"tweet": tweet, "score": sentiment_dict["compound"], "time": row["_id"]})

                if -0.5 > sentiment_dict['compound'] >= -0.75:
                    count = count + sentiment_dict['compound']
                    cnt = cnt + 1
                    for sentiment in sentiment_list:
                        if sentiment["sentiment"] == "sell":
                            sentiment["tweets"].append(
                                {"tweet": tweet, "score": sentiment_dict["compound"], "time": row["_id"]})

                if sentiment_dict['compound'] < -0.75:
                    count = count + sentiment_dict['compound']
                    cnt = cnt + 1
                    for sentiment in sentiment_list:
                        if sentiment["sentiment"] == "strong_sell":
                            sentiment["tweets"].append(
                                {"tweet": tweet, "score": sentiment_dict["compound"], "time": row["_id"]})

            #             hour_sentiment["count"] = (count * 100) / len(row["text"])
            total_sentiment.append([(count * 100) / len(row["text"]), max])

            #             hour_sentiment["count"] = ((count * 100) / cnt)
            total_sentiment2.append([((count * 100) / cnt), max])
            # tokens = nltk.word_tokenize(processed)
            # lower_case = [l.lower() for l in tokens]
            # filtered_result = list(filter(lambda l: l not in STOPWORDS, lower_case))
            # emmas = [wordnet_lemmatizer.lemmatize(t) for t in filtered_result]
            # time_lema["words"].append(lemmas)

            # time_list.append(time_lema)

    except StopIteration:
        print("Error loading tweets from MongoDB!")
    # sys.stdout.flush()
    # sys.stdout.write("\rDocuments processed: " + "{:.2%}\n".format(count / len(my_doc)))
    return 0


# In[8]:


if __name__ == "__main__":
    sentiment_list.append({"sentiment": "buy", "tweets": []})
    sentiment_list.append({"sentiment": "strong_buy", "tweets": []})
    sentiment_list.append({"sentiment": "sell", "tweets": []})
    sentiment_list.append({"sentiment": "strong_sell", "tweets": []})

# In[9]:


# docs = load_tweets(1515416400) #19.1.2018
# for item in total_sentiment:
# print(item)


# In[10]:


docs = load_tweets(1413602000)  # 18.12.2017

# In[82]:


#  for item in total_sentiment:
#          print(str(item[0]) + "  " + str(item[1]))


# In[81]:


#  for item in total_sentiment2:
#          print(str(item[0]) + "  " + str(item[1]))


# In[155]:


load_test_tweets()

# In[80]:


# for item in test_total_sentiment2:
#          print(str(item[0]) + "  " + str(item[1]))


# In[156]:


print(test_verdicts)

# In[283]:


svclassifier = SVC(kernel='rbf', gamma='auto', C=1E6, class_weight='balanced')
svclassifier.fit(total_sentiment2, verdicts)

# In[284]:


y_pred = svclassifier.predict(test_total_sentiment2)

# In[285]:


print(confusion_matrix(test_verdicts, y_pred))
print(classification_report(test_verdicts, y_pred))

# In[270]:


plt.scatter(np.array(total_sentiment2)[:, 0], np.array(total_sentiment2)[:, 1], c=verdicts, s=50, cmap='viridis');
plt.scatter(svclassifier.support_vectors_[:, 0], svclassifier.support_vectors_[:, 1], s=5, lw=1);

# In[271]:


plt.scatter(np.array(test_total_sentiment)[:, 0], np.array(test_total_sentiment)[:, 1], c=test_verdicts, s=50,
            cmap='viridis');

# In[272]:


# svclassifier.support_vectors_


# In[273]:


plt.scatter(svclassifier.support_vectors_[:, 0], svclassifier.support_vectors_[:, 1], s=5, lw=1);

# In[ ]:





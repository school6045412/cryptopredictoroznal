from Streamer import CustomStreamer
from threading import Thread
import GetOldTweets3 as got
import pymongo
import json
from datetime import datetime, date, timedelta, timezone
import time
import numpy as np
import pandas as pd
import dateutil.parser
import re
from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt
import matplotlib
import winsound

untilDate = '2019-1-1'
tweetList = []

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["twitter"]
mycol = mydb["python_learn"]


def default(o):
    if isinstance(o, (date, datetime)):
        return o.isoformat()


def loadTweets():
    latesttime = "2019-02-28"
    fromtime = "2019-02-27"
    starttime = "2018-03-01"

    mydoc = list(mycol.find({}).sort("date", 1).limit(1))
    try:
        if mydoc:
            record = mydoc[0]
            latesttime = datetime.utcfromtimestamp(record["date"]).strftime('%Y-%m-%d')
            fromtime = (datetime.utcfromtimestamp(record["date"]) - timedelta(days=1)).strftime('%Y-%m-%d')
    except StopIteration:
        print("Empty cursor!")

    while starttime != latesttime:
        #winsound.PlaySound("SystemExit", winsound.SND_ALIAS)
        #wordcloud(processTweets(mydoc))
        print("pulling tweets from " + fromtime + " to " + latesttime)
        print("starting at time: " + datetime.now().strftime("%Y-%m-%d %H:%M"))
        start = time.time()
        tweetCriteria = got.manager.TweetCriteria() \
            .setQuerySearch("bitcoin") \
            .setSince(fromtime) \
            .setUntil(latesttime) \
            .setMaxTweets(100000)
        try:
            tweets = got.manager.TweetManager.getTweets(tweetCriteria)
        except Exception:
            tweets = got.manager.TweetManager.getTweets(tweetCriteria)
        dblist = []
        for line in tweets:
            temp = line.__getattribute__("date").replace(tzinfo=timezone.utc).timestamp()
            line.__setattr__("date", int(round(temp)))
            dblist.append(json.loads(json.dumps(line.__dict__, default=default)))
        mycol.insert_many(dblist)
        end = time.time()
        print("time elapsed: " + str(end - start))
        latesttime = tweets[-1].__getattribute__("date")
        latesttime = datetime.utcfromtimestamp(latesttime).strftime('%Y-%m-%d')
        fromtime = (tweets[-1].__getattribute__("date"))
        fromtime = (datetime.utcfromtimestamp(fromtime)- timedelta(days=1)).strftime('%Y-%m-%d')
        print("inserted " + str(len(tweets)) + " records")


def processTweets(tweets):
    processed = []
    for tweet in tweets:
        try:
            tweet["text"] = " ".join([word for word in tweet["text"].split()
                                          if 'http' not in word and '@' not in word and '<' not in word and 'pic.twitter.com' not in word])
            tweet["text"] = re.sub('[!@#$()".;,?&=]', '', tweet["text"])
            tweet["text"] = re.sub(':', ' ', tweet["text"])
            tweet["text"] = re.sub('  ', ' ', tweet["text"])
            tweet["text"] = tweet["text"].lower()
            processed.append(tweet["text"])
        except StopIteration:
            print("error occurred!")
    return processed


def wordcloud(tweets):
    stopwords = set(STOPWORDS)
    wordcloud = WordCloud(background_color="white", collocations=False, stopwords=stopwords).generate(" ".join([i for i in tweets]))
    plt.figure(figsize=(20, 10), facecolor='k')
    plt.axis("off")
    plt.title("WordCloud Bitcoin")
    plt.savefig('test.eps', format='eps', dpi=900)
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.show()


if __name__ == "__main__":
    loadTweets()

#in case, MongoDB does not insert date as timestamp ;)
#db.getCollection('python_test').find({}).forEach(function(row){
#    var ts = new Date(row.date).getTime();
#    db.getCollection('python_test').update({_id: row._id}, {$set: {"date": NumberLong(ts)}})
#    })
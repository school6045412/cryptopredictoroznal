from twython import TwythonStreamer
import csv


class CustomStreamer(TwythonStreamer):
    flag = True
    temp1 = []

    def on_success(self, data):
        if 'text' and data['retweeted'] in data:
            with open('test_dataset.csv', 'w') as csvFile:
                filewriter = csv.DictWriter(csvFile, ['created_at', 'text', 'user'])
                #print(data['text'])
                filewriter.writerow({'created_at': data['created_at'], 'text': data['text'].encode('utf-8'), 'user': data['user']['name']})

    def on_error(self, status_code, data):
        print(status_code)
        self.disconnect()

from flask import Flask, flash, redirect, render_template, request, session, Session, url_for
from bson import json_util
from Streamer import CustomStreamer
from threading import Thread
from twython import Twython
import GetOldTweets3 as got
import pymongo
import json
from datetime import datetime, date, timedelta
import time
import numpy as np
import pandas as pd
import dateutil.parser
import re
from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt
import matplotlib
import winsound

app = Flask(__name__)
app.config['SECRET_KEY'] = 'fUtPIXsC035Ez4o12qF5mfWxPKH6YnHbzxXVX01AbzbQ4AjGYS'

TWITTER_APP_KEY = 'Z51WDpDLbLedwSxXkDJajmaFk' #supply the appropriate value
TWITTER_APP_KEY_SECRET = 'fUtPIXsC035Ez4o12qF5mfWxPKH6YnHbzxXVX01AbzbQ4AjGYS'
TWITTER_ACCESS_TOKEN = '1096425960725991424-ZnPNwDWXYed3jdJ3Cr2XvmyNsgAE8W'
TWITTER_ACCESS_TOKEN_SECRET = '8P0ReN6WypcxAXzIOXAQmL00YN2c8qVNwyjcTBRemXpXt'

ethKeywords = ['btc', 'bitcoin']
untilDate = '2019-1-1'
tweetList = []

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["twitter"]
mycol = mydb["python_test"]


@app.route('/stream')
def catchbtcstreaming():
    oauth_verifier = request.args.get('oauth_verifier')
    OAUTH_TOKEN = session['OAUTH_TOKEN']
    OAUTH_TOKEN_SECRET = session['OAUTH_TOKEN_SECRET']
    twitter = Twython(TWITTER_APP_KEY, TWITTER_APP_KEY_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
    final_step = twitter.get_authorized_tokens(oauth_verifier)
    session['OAUTH_TOKEN'] = final_step['oauth_token']
    session['OAUTH_TOKEN_SECRET'] = final_step['oauth_token_secret']
    OAUTH_TOKEN = session['OAUTH_TOKEN']
    OAUTH_TOKEN_SECRET = session['OAUTH_TOKEN_SECRET']
    session['twitter'] = CustomStreamer(TWITTER_APP_KEY, TWITTER_APP_KEY_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)

    session['twitter'].statuses.filter(track='bitcoin')


@app.route('/', methods=["GET", "POST"])
def init():
    loadTweets()
    #if request.method == 'POST':
    twitter = Twython(TWITTER_APP_KEY, TWITTER_APP_KEY_SECRET)
    auth = twitter.get_authentication_tokens(callback_url='http://localhost:5000/stream')
    session['OAUTH_TOKEN'] = auth['oauth_token']
    session['OAUTH_TOKEN_SECRET'] = auth['oauth_token_secret']
    return redirect(auth['auth_url'])


def default(o):
    if isinstance(o, (date, datetime)):
        return o.isoformat()


def loadTweets():
    latesttime = "2018-02-28"
    fromtime = "2018-02-27"
    starttime = "2017-10-01"

    mydoc = list(mycol.find({}).sort("date", 1))
    try:
        record = mydoc[0]
        basetime = dateutil.parser.parse(record["date"])
        latesttime = dateutil.parser.parse(record["date"]).strftime('%Y-%m-%d')
        fromtime = (dateutil.parser.parse(record["date"]) - timedelta(days=1)).strftime('%Y-%m-%d')
    except StopIteration:
        print("Empty cursor!")

    while starttime != latesttime:
        winsound.PlaySound("SystemExit", winsound.SND_ALIAS)
        wordcloud(processTweets(mydoc))
        print("pulling tweets from " + fromtime + " to " + latesttime)
        start = time.time()
        tweetCriteria = got.manager.TweetCriteria() \
            .setQuerySearch("bitcoin") \
            .setSince(fromtime) \
            .setUntil(latesttime) \
            .setMaxTweets(100000)
        tweets = got.manager.TweetManager.getTweets(tweetCriteria)

        dblist = []
        for line in tweets:
            dblist.append(json.loads(json.dumps(line.__dict__, default=default)))
        mycol.insert_many(dblist)
        end = time.time()
        print("time elapsed: " + str(end - start))
        latesttime = tweets[-1].__getattribute__("date").strftime('%Y-%m-%d')
        fromtime = (tweets[-1].__getattribute__("date") - timedelta(days=1)).strftime('%Y-%m-%d')
        print("inserted " + str(len(tweets)) + " records")


def processTweets(tweets):
    processed = []
    for tweet in tweets:
        try:
            tweet["text"] = " ".join([word for word in tweet["text"].split()
                                          if 'http' not in word and '@' not in word and '<' not in word and 'pic.twitter.com' not in word])
            tweet["text"] = re.sub('[!@#$()".;,?&=]', '', tweet["text"])
            tweet["text"] = re.sub(':', ' ', tweet["text"])
            tweet["text"] = re.sub('  ', ' ', tweet["text"])
            tweet["text"] = tweet["text"].lower()
            processed.append(tweet["text"])
        except StopIteration:
            print("error occurred!")
    return processed


def wordcloud(tweets):
    stopwords = set(STOPWORDS)
    wordcloud = WordCloud(background_color="white", collocations=False, stopwords=stopwords).generate(" ".join([i for i in tweets]))
    plt.figure(figsize=(20, 10), facecolor='k')
    plt.axis("off")
    plt.title("WordCloud Bitcoin")
    plt.savefig('test.eps', format='eps', dpi=900)
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.show()


if __name__ == "__main__":
    app.run(debug=True)

    #thread = Thread(target=catchbtcstreaming)
    #thread.start()
    #thread.join()
